#!/usr/bin/env python
# Created by P. Mendez 30/03/2020
# Code intended to prepare the input file containing data using as input just the excel file

import os
import sys, argparse
import shutil
import xlrd
import re
from datetime import datetime

url = "https://epdtdi-elog.cern.ch"

substitutions = {"\\"+"n": "", "xa0": "", "\\"+"xe8me": "eme", "\\": " ", "xe0": "e", "xe9": "e", "u'": "'", "[": "", "]": "", "u\"": "","xa9": "","xc3": ""}
def clean_entries(string, substitutions):

    substrings = sorted(substitutions, key=len, reverse=True)
    regex = re.compile('|'.join(map(re.escape, substrings)))
    return regex.sub(lambda match: substitutions[match.group(0)], string)

def main():
    os.environ["TDAQ_PYTHONPATH"] = "/home/pmendez/workspace/python"
    os.environ["PYTHONPATH"] = "/home/pmendez/workspace/python"
    os.environ["USE_ELISA_MERGED"] = "true"

    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--infile', help="excel file to parse", default='', dest='inputfile')
    args = parser.parse_args()

    if not args.inputfile:
        raise RuntimeError("inputfile -i(--infile) option must be specified")

    loc = (args.inputfile)

    wb = xlrd.open_workbook(loc)
    sheet = wb.sheet_by_index(0)
    sheet.cell_value(0, 0)

    for i in range(sheet.nrows):
        
        entries = sheet.row_values(i)
        for j in range(len(sheet.row_values(i))):
            if (not type(entries[j])==float):
                entries[j]=entries[j].encode('utf-8')
                if (j==6):
                    entries[j]=clean_entries(entries[j],substitutions)
            else:
                if (j==3 or j== 4):
#                    entries[j] = xlrd.xldate_as_tuple(entries[j], 0)
                    y, m, d, h, i, s =  xlrd.xldate_as_tuple(entries[j], 0)
                    entries[j]=("{0}/{1}/{2} {3}:{4}".format(y, m, d, h, i))
                else:   
                    entries[j]=entries[j]


        print entries[3]
        d={ 'entry':entries[0], 'view':entries[1], 'author':entries[2], 'start':entries[3], 'end':entries[4], 'subject':entries[5], 'text':entries[6], 'contact':entries[7], 'moyen':entries[8], 'system':entries[9], 'rest':entries[10] }

        print ("-------------------------------")

if __name__ == "__main__":
  main()
