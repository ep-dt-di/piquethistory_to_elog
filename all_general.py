#!/usr/bin/env python
# Created by P. Mendez 30/03/2020
# Code intended to prepare the input file containing data using as input just the excel file

import os
import subprocess
import sys, argparse

import shutil
import xlrd
import re
import unicodedata
import math
from datetime import datetime



substitutions = {"\\"+"n": "", "xa0": "", "\\"+"xe8me": "eme", "\\": " ", "xe0": "e", "xe9": "e", "u'": "'", "[": "", "]": "", "u\"": "","xa9": "","0xc3": "","xe8":""}
def clean_entries(string, substitutions):

    substrings = sorted(substitutions, key=len, reverse=True)
    regex = re.compile('|'.join(map(re.escape, substrings)))
    return regex.sub(lambda match: substitutions[match.group(0)], string)

def xldate_to_datetime(xldatetime): #something like 43705.6158241088
      import math
      import datetime
      tempDate = datetime.datetime(1899, 12, 31)
      (days, portion) = math.modf(xldatetime)

      deltaDays = datetime.timedelta(days=days)
      #changing the variable name in the edit
      secs = int(24 * 60 * 60 * portion)
      detlaSeconds = datetime.timedelta(seconds=secs)
      TheTime = (tempDate + deltaDays + detlaSeconds )
      return TheTime.strftime("%d-%m-%Y %H:%M")


def main():
    os.environ["TDAQ_PYTHONPATH"] = "/home/pmendez/workspace/python"
    os.environ["PYTHONPATH"] = "/home/pmendez/workspace/python"
    os.environ["USE_ELISA_MERGED"] = "true" 
    url = "https://epdtdi-piquet-elog.cern.ch"
    logbook="epdtdi-piquet"

    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--infile', help="excel file to parse", default='', dest='inputfile')
    parser.add_argument('-e', '--experiment', help="experiment name", default='', dest='experiment_name')
    parser.add_argument('-m', '--message', help="experiment name", default='', dest='message_type')
    args = parser.parse_args()

    if not args.inputfile:
        raise RuntimeError("inputfile -i(--infile) option must be specified")

    if not args.experiment_name:
        raise RuntimeError("experiment name -e(--experiment) option must be specified")

    if not args.message_type:
        raise RuntimeError("message_type -m(--message) option must be specified")



    loc = (args.inputfile)
    exp = (args.experiment_name)
    message = (args.message_type)

    wb = xlrd.open_workbook(loc)
    sheet = wb.sheet_by_index(0)
    sheet.cell_value(0, 0)
    
    for i in range(sheet.nrows):
        
        entries = sheet.row_values(i)
        for j in range(len(sheet.row_values(i))):
            if (not type(entries[j])==float):
                entries[j]=entries[j].encode('utf-8')
            else:
                if (j==3 or j== 4):
                    entries[j] = xldate_to_datetime(entries[j])
                else:   
                    entries[j]=entries[j]

 

        entries[3] = entries[3]+":00"
        entries[4] = entries[4]+":00"
        if (entries[5]=="" or exp == "ALICE"):
            entries[5] = "empty subject" 
        if (entries[7]=="Central Solenoids" or entries[7]=="Central Solenoid"):
            entries[7] = "Central solenoid"
        if (entries[7]=="Toroids"):
            entries[7] = "Toroid"
        if (entries[9]=="SLIMOS"):
            entries[9] = "GLIMOS/SLIMOS"  
        if (entries[9]=="Shift Leader"):
            entries[9] = "Shift leader"    
        if (entries[9]=="Technical Coordinator"):
            entries[9] = "Technical coordinator"    
        if (entries[8]=="Other"):
            entries[8] = "N/A"
        if(entries[9]=="Other"):
            entries[9]="None"
        if(entries[7]=="Other"):
            entries[7]="Any"

        d={ 'entry':entries[0], 'view':entries[1], 'author':entries[2], 'start':entries[3], 'end':entries[4], 'subject':entries[5], 'text':entries[6], 'contact':entries[7], 'moyen':entries[8], 'system':entries[9], 'rest':entries[10] }


        print "contact   ", entries[7] # for me item_affected 
        print "moyen   ", entries[8] # for me subsystem
        print "system   ", entries[9] # for me contact

################
        if (entries[7]=="To & CS" and entries[8]=="Vacuum"):
            print "CASE2"
            elisa_insert = 'python /home/pmendez/workspace/python/elisa_client_api/elisa_insert.py  -v 4 -o /home/pmendez/workspace/python/cookie.txt -s "%s" -j "%s" -a "%s" -y "%s" -b "%s" -p "StartTime=%s" -p "EndTime=%s" -p "%s_item_affected=Central solenoid" -p "%s_entry_type=%s" -x "closed" -p "%s_subsystem=N/A" -e "Magnets" -p "Contact=%s" -k "%s"' %(url,d['subject'], d['author'],exp,d['text'],d['start'],d['end'],exp,exp,message,exp,d['system'],logbook)
        elif (entries[7]=="To & CS" and entries[8]!="Vacuum"):
            print "CASE2bis"
            elisa_insert = 'python /home/pmendez/workspace/python/elisa_client_api/elisa_insert.py  -v 4 -o /home/pmendez/workspace/python/cookie.txt -s "%s" -j "%s" -a "%s" -y "%s" -b "%s" -p "StartTime=%s" -p "EndTime=%s" -p "%s_item_affected=Central solenoid" -p "%s_entry_type=%s" -x "closed" -p "%s_subsystem=%s" -e "Magnets" -p "Contact=%s" -k "%s"' %(url,d['subject'], d['author'],exp,d['text'],d['start'],d['end'],exp,exp,message,exp,d['moyen'],d['system'],logbook)
 
###############################################3
        elif (entries[7]=="Toroid" and entries[8]=="Vacuum"):
            print "CASE3"
            elisa_insert = 'python /home/pmendez/workspace/python/elisa_client_api/elisa_insert.py -v 4  -o /home/pmendez/workspace/python/cookie.txt -s "%s"  -j "%s" -a "%s" -y "%s"  -b "%s" -p "StartTime=%s" -p "EndTime=%s" -p "%s_item_affected=Toroid" -p "Contact=%s" -p "%s_subsystem=N/A" -x "closed" -p "%s_entry_type=%s" -k "%s" -e "Magnets"' %(url,d['subject'], d['author'],exp,d['text'],d['start'],d['end'],exp,d['system'],exp,exp,message,logbook)
        elif (entries[7]=="Toroid" and entries[8]!="Vacuum"):
            print "CASE3bis"
            elisa_insert = 'python /home/pmendez/workspace/python/elisa_client_api/elisa_insert.py -v 4  -o /home/pmendez/workspace/python/cookie.txt -s "%s"  -j "%s" -a "%s" -y "%s"  -b "%s" -p "StartTime=%s" -p "EndTime=%s" -p "%s_item_affected=Toroid" -p "Contact=%s" -p "%s_subsystem=%s" -x "closed" -p "%s_entry_type=%s" -k "%s" -e "Magnets"' %(url,d['subject'], d['author'],exp,d['text'],d['start'],d['end'],exp,d['system'],exp,d['moyen'],exp,message,logbook)

###################################################
        elif (entries[7]=="Central solenoid"):
            print "CASE3.1"
            elisa_insert = 'python /home/pmendez/workspace/python/elisa_client_api/elisa_insert.py -v 4 -o /home/pmendez/workspace/python/cookie.txt -s "%s"  -j "%s" -a "%s" -y "%s"  -b "%s" -p "StartTime=%s" -p "EndTime=%s" -p "%s_item_affected=Central solenoid" -p "Contact=%s" -p "%s_subsystem=%s" -x "closed" -p "%s_entry_type=%s" -k "%s" -e "Magnets"' %(url,d['subject'], d['author'],exp,d['text'],d['start'],d['end'],exp,d['system'],exp,d['moyen'],exp,message,logbook)

########################
        elif (entries[8]=="Vacuum" and entries[7]!="Toroid"):
            print "CASE4"
            elisa_insert = 'python /home/pmendez/workspace/python/elisa_client_api/elisa_insert.py -v 4 -o /home/pmendez/workspace/python/cookie.txt -s "%s" -j "%s" -a "%s" -y "%s" -b "%s" -p "StartTime=%s" -p "EndTime=%s"  -p "Contact=%s" -p "%s_subsystem=N/A" -p "%s_entry_type=%s" -p "%s_item_affected=Vacuum" -x "closed" -e "Magnets" -k "%s"'  %(url,d['subject'], d['author'],exp,d['text'],d['start'],d['end'],d['system'],exp,exp,message,exp,logbook)

############################
        elif (entries[8]=="DSS"):
            print "CASE5"
            elisa_insert = 'python /home/pmendez/workspace/python/elisa_client_api/elisa_insert.py -v 4 -o /home/pmendez/workspace/python/cookie.txt -s "%s" -j "%s" -a "%s" -y "%s" -b "%s" -p "StartTime=%s"  -p "EndTime=%s"  -p "Contact=%s" -p "%s_item_affected=Any" -p "%s_entry_type=%s" -p "%s_subsystem=N/A" -x "closed" -e "DSS" -k "%s"'  %(url,d['subject'], d['author'],exp,d['text'],d['start'],d['end'],d['system'],exp,exp,message,exp,logbook)

#########################################
        elif (entries[8]=="MCS"):
            print "CASE6"
            elisa_insert = 'python /home/pmendez/workspace/python/elisa_client_api/elisa_insert.py -v 4 -o /home/pmendez/workspace/python/cookie.txt -s "%s" -j "%s" -a "%s" -y "%s" -b "%s" -p "StartTime=%s"  -p "EndTime=%s"  -p "Contact=%s" -p "%s_item_affected=%s" -p "%s_entry_type=%s" -p "%s_subsystem=MCS" -x "closed" -e "Magnets" -k "%s"'  %(url,d['subject'], d['author'],exp,d['text'],d['start'],d['end'],d['system'],exp,d['moyen'],exp,message,exp,logbook)
        elif (entries[8]=="MSS"):
            print "CASE7"
            elisa_insert = 'python /home/pmendez/workspace/python/elisa_client_api/elisa_insert.py -v 4 -o /home/pmendez/workspace/python/cookie.txt -s "%s" -j "%s" -a "%s" -y "%s" -b "%s" -p "StartTime=%s"  -p "EndTime=%s"  -p "Contact=%s" -p "%s_item_affected=%s" -p "%s_entry_type=%s" -p "%s_subsystem=MSS" -x "closed" -e "Magnets" -k "%s"'  %(url,d['subject'], d['author'],exp,d['text'],d['start'],d['end'],d['system'],exp,d['moyen'],exp,message,exp,logbook)
        elif (entries[8]=="RCS"):
            print "CASE8"
            elisa_insert = 'python /home/pmendez/workspace/python/elisa_client_api/elisa_insert.py -v 4 -o /home/pmendez/workspace/python/cookie.txt -s "%s" -j "%s" -a "%s" -y "%s" -b "%s" -p "StartTime=%s"  -p "EndTime=%s"  -p "Contact=%s" -p "%s_item_affected=%s" -p "%s_entry_type=%s" -p "%s_subsystem=RCS" -x "closed" -e "DSS" -k "%s"'  %(url,d['subject'], d['author'],exp,d['text'],d['start'],d['end'],d['system'],exp,d['moyen'],exp,message,exp,logbook)
            

##########
        else:
            print "ELSE3"
            elisa_insert = 'python /home/pmendez/workspace/python/elisa_client_api/elisa_insert.py -v 4 -o /home/pmendez/workspace/python/cookie.txt -s "%s"  -j "%s" -a "%s" -y "%s"  -b "%s" -p "StartTime=%s" -p "EndTime=%s" -p "%s_item_affected=%s" -p "Contact=%s" -p "%s_subsystem=%s" -x "closed" -p "%s_entry_type=%s" -k "%s" -e "Other"' %(url,d['subject'], d['author'],exp,d['text'],d['start'],d['end'],exp,d['contact'],d['system'],exp,d['moyen'],exp,message,logbook)


        print elisa_insert    

        if (entries[5] != "Titre"):
            cmd = subprocess.Popen(elisa_insert, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
            output = cmd.communicate()[0]
            print output            

            value_id=None

            for line in output.split('\n'):
                if line.startswith("id") and not line.startswith("idem") and line.find("None")==-1:
                    value_id = int(line.split(":")[1].lstrip())

            print "tttttttttttttttttttttttttttt", value_id
            if(value_id):
                elisa_update = 'python /home/pmendez/workspace/python/elisa_client_api/elisa_update.py -v 4 -o /home/pmendez/workspace/python/cookie.txt -s %s -k %s -i %d -d "%s"'  %(url, logbook, value_id, d['end'])
                cmd2 = subprocess.Popen(elisa_update, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
                print elisa_update
                output2 = cmd2.communicate()[0]
                print output2

        print ("------------------------------- ENTRY COMPLETED--------------------------------------------------------")

if __name__ == "__main__":
  main()
