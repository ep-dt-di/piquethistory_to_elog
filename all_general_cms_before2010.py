#!/usr/bin/env python
# Created by P. Mendez 30/03/2020
# Code intended to prepare the input file containing data using as input just the excel file

import os
import subprocess
import sys, argparse

import shutil
import xlrd
import re
import unicodedata
import math
from datetime import datetime



substitutions = {"\\"+"n": "", "xa0": "", "\\"+"xe8me": "eme", "\\": " ", "xe0": "e", "xe9": "e", "u'": "'", "[": "", "]": "", "u\"": "","xa9": "","0xc3": "","xe8":""}
def clean_entries(string, substitutions):

    substrings = sorted(substitutions, key=len, reverse=True)
    regex = re.compile('|'.join(map(re.escape, substrings)))
    return regex.sub(lambda match: substitutions[match.group(0)], string)

def xldate_to_datetime(xldatetime): #something like 43705.6158241088
      import math
      import datetime
      tempDate = datetime.datetime(1899, 12, 31)
      (days, portion) = math.modf(xldatetime)

      deltaDays = datetime.timedelta(days=days)
      #changing the variable name in the edit
      secs = int(24 * 60 * 60 * portion)
      detlaSeconds = datetime.timedelta(seconds=secs)
      TheTime = (tempDate + deltaDays + detlaSeconds )
      return TheTime.strftime("%d-%m-%Y %H:%M")


def main():
    os.environ["TDAQ_PYTHONPATH"] = "/home/pmendez/workspace/python"
    os.environ["PYTHONPATH"] = "/home/pmendez/workspace/python"
    os.environ["USE_ELISA_MERGED"] = "true" 
    url = "https://epdtdi-piquet-elog.cern.ch"
    logbook="epdtdi-piquet"

    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--infile', help="excel file to parse", default='', dest='inputfile')
    parser.add_argument('-e', '--experiment', help="experiment name", default='', dest='experiment_name')
    parser.add_argument('-m', '--message', help="experiment name", default='', dest='message_type')
    args = parser.parse_args()

    if not args.inputfile:
        raise RuntimeError("inputfile -i(--infile) option must be specified")

    if not args.experiment_name:
        raise RuntimeError("experiment name -e(--experiment) option must be specified")

    if not args.message_type:
        raise RuntimeError("message_type -m(--message) option must be specified")



    loc = (args.inputfile)
    exp = (args.experiment_name)
    message = (args.message_type)

    wb = xlrd.open_workbook(loc)
    sheet = wb.sheet_by_index(0)
    sheet.cell_value(0, 0)
    
    for i in range(sheet.nrows):
        
        entries = sheet.row_values(i)
        for j in range(len(sheet.row_values(i))):
            if (not type(entries[j])==float):
                entries[j]=entries[j].encode('utf-8')
            else:
                if (j==2):
                    entries[j] = xldate_to_datetime(entries[j])
                else:   
                    entries[j]=entries[j]

 

        entries[2] = entries[2]+":00"

        if (entries[5]=="Other"):
            entries[5] = "N/A"
        if(entries[4]=="Other"):
            entries[4]="Any"

        d={ 'entry':entries[0], 'view':entries[1], 'author':entries[1], 'start':entries[2], 'end':entries[2], 'subject':"empty subject", 'text':entries[3], 'contact':"Any", 'moyen':entries[4], 'system':entries[5]}


        print "4    ", entries[4] # for me item_affected 
        print "5   ", entries[5] # for me subsystem


        if (entries[4]=="Vacuum"):
            print "CASE4"
            elisa_insert = 'python /home/pmendez/workspace/python/elisa_client_api/elisa_insert.py -v 4 -o /home/pmendez/workspace/python/cookie.txt -s "%s" -j "%s" -a "%s" -y "%s" -b "%s" -p "StartTime=%s" -p "EndTime=%s"  -p "Contact=%s" -p "%s_subsystem=N/A" -p "%s_entry_type=%s" -p "%s_item_affected=Vacuum" -x "closed" -e "Magnets" -k "%s"'  %(url,d['subject'], d['author'],exp,d['text'],d['start'],d['end'],d['contact'],exp,exp,message,exp,logbook)

############################
        elif (entries[4]=="DSS"):
            print "CASE5"
            elisa_insert = 'python /home/pmendez/workspace/python/elisa_client_api/elisa_insert.py -v 4 -o /home/pmendez/workspace/python/cookie.txt -s "%s" -j "%s" -a "%s" -y "%s" -b "%s" -p "StartTime=%s"  -p "EndTime=%s"  -p "Contact=%s" -p "%s_item_affected=Any" -p "%s_entry_type=%s" -p "%s_subsystem=N/A" -x "closed" -e "DSS" -k "%s"'  %(url,d['subject'], d['author'],exp,d['text'],d['start'],d['end'],d['contact'],exp,exp,message,exp,logbook)

#########################################
        elif (entries[4]=="MCS"):
            print "CASE6"
            elisa_insert = 'python /home/pmendez/workspace/python/elisa_client_api/elisa_insert.py -v 4 -o /home/pmendez/workspace/python/cookie.txt -s "%s" -j "%s" -a "%s" -y "%s" -b "%s" -p "StartTime=%s"  -p "EndTime=%s"  -p "Contact=%s" -p "%s_item_affected=Any" -p "%s_entry_type=%s" -p "%s_subsystem=MCS" -x "closed" -e "Magnets" -k "%s"'  %(url,d['subject'], d['author'],exp,d['text'],d['start'],d['end'],d['contact'],exp,exp,message,exp,logbook)
        elif (entries[4]=="MSS"):
            print "CASE7"
            elisa_insert = 'python /home/pmendez/workspace/python/elisa_client_api/elisa_insert.py -v 4 -o /home/pmendez/workspace/python/cookie.txt -s "%s" -j "%s" -a "%s" -y "%s" -b "%s" -p "StartTime=%s"  -p "EndTime=%s"  -p "Contact=%s" -p "%s_item_affected=Any" -p "%s_entry_type=%s" -p "%s_subsystem=MSS" -x "closed" -e "Magnets" -k "%s"'  %(url,d['subject'], d['author'],exp,d['text'],d['start'],d['end'],d['contact'],exp,exp,message,exp,logbook)
        elif (entries[4]=="RCS"):
            print "CASE8"
            elisa_insert = 'python /home/pmendez/workspace/python/elisa_client_api/elisa_insert.py -v 4 -o /home/pmendez/workspace/python/cookie.txt -s "%s" -j "%s" -a "%s" -y "%s" -b "%s" -p "StartTime=%s"  -p "EndTime=%s"  -p "Contact=%s" -p "%s_item_affected=Any" -p "%s_entry_type=%s" -p "%s_subsystem=RCS" -x "closed" -e "DSS" -k "%s"'  %(url,d['subject'], d['author'],exp,d['text'],d['start'],d['end'],d['contact'],exp,exp,message,exp,logbook)
            

##########
        else:
            print "ELSE3"
            elisa_insert = 'python /home/pmendez/workspace/python/elisa_client_api/elisa_insert.py -v 4 -o /home/pmendez/workspace/python/cookie.txt -s "%s"  -j "%s" -a "%s" -y "%s"  -b "%s" -p "StartTime=%s" -p "EndTime=%s" -p "%s_item_affected=Any" -p "Contact=%s" -p "%s_subsystem=%s" -x "closed" -p "%s_entry_type=%s" -k "%s" -e "Other"' %(url,d['subject'], d['author'],exp,d['text'],d['start'],d['end'],exp,d['contact'],exp,d['moyen'],exp,message,logbook)


        print elisa_insert    

        if (entries[1] != "Auteur"):
            cmd = subprocess.Popen(elisa_insert, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
            output = cmd.communicate()[0]
            print output            

            value_id=None

            for line in output.split('\n'):
                if line.startswith("id") and not line.startswith("idem") and line.find("None")==-1:
                    value_id = int(line.split(":")[1].lstrip())
            print "tttttttttttttttttttttttttttt", value_id
            if(value_id):
                elisa_update = 'python /home/pmendez/workspace/python/elisa_client_api/elisa_update.py -v 4 -o /home/pmendez/workspace/python/cookie.txt -s %s -k %s -i %d -d "%s"'  %(url, logbook, value_id, d['end'])
                cmd2 = subprocess.Popen(elisa_update, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
                print elisa_update
                output2 = cmd2.communicate()[0]
                print output2

        print ("------------------------------- ENTRY COMPLETED--------------------------------------------------------")

if __name__ == "__main__":
  main()
