#!/usr/bin/env python
# Created by P. Mendez 30/03/2020
# Code intended to prepare the input file containing data using as input just the excel file

import os
import subprocess
import sys, argparse

import shutil
import xlrd
import re
import unicodedata
import math
from datetime import datetime

url = "https://epdtdi-elog.cern.ch"

substitutions = {"\\"+"n": "", "xa0": "", "\\"+"xe8me": "eme", "\\": " ", "xe0": "e", "xe9": "e", "u'": "'", "[": "", "]": "", "u\"": "","xa9": "","0xc3": "","xe8":""}
def clean_entries(string, substitutions):

    substrings = sorted(substitutions, key=len, reverse=True)
    regex = re.compile('|'.join(map(re.escape, substrings)))
    return regex.sub(lambda match: substitutions[match.group(0)], string)

def xldate_to_datetime(xldatetime): #something like 43705.6158241088
      import math
      import datetime
      tempDate = datetime.datetime(1899, 12, 31)
      (days, portion) = math.modf(xldatetime)

      deltaDays = datetime.timedelta(days=days)
      #changing the variable name in the edit
      secs = int(24 * 60 * 60 * portion)
      detlaSeconds = datetime.timedelta(seconds=secs)
      TheTime = (tempDate + deltaDays + detlaSeconds )
      return TheTime.strftime("%d-%m-%Y %H:%M")


def main():
    os.environ["TDAQ_PYTHONPATH"] = "/home/pmendez/workspace/python"
    os.environ["PYTHONPATH"] = "/home/pmendez/workspace/python"
    os.environ["USE_ELISA_MERGED"] = "true" 

    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--infile', help="excel file to parse", default='', dest='inputfile')
    args = parser.parse_args()

    if not args.inputfile:
        raise RuntimeError("inputfile -i(--infile) option must be specified")

    loc = (args.inputfile)

    wb = xlrd.open_workbook(loc)
    sheet = wb.sheet_by_index(0)
    sheet.cell_value(0, 0)
    
    for i in range(sheet.nrows):
        
        entries = sheet.row_values(i)
        for j in range(len(sheet.row_values(i))):
            if (not type(entries[j])==float):
                entries[j]=entries[j].encode('utf-8')
            else:
                if (j==3 or j== 4):
#                    y, m, d, h, i, s =  xlrd.xldate_as_tuple(entries[j], 0)
#                    if (i == 0):
#                        i="00"
#                    entries[j] = datetime.datetime(y, m, d, h, i, s)
                    entries[j] = xldate_to_datetime(entries[j])
                else:   
                    entries[j]=entries[j]

 

        entries[3] = entries[3]+":00"
        entries[4] = entries[4]+":00"
        if (entries[5]==""):
            entries[5] = "empty subject" 
        if (entries[7]=="Toroids"):
            entries[7] = "Toroid"
        if (entries[9]=="SLIMOS"):
            entries[9] = "GLIMOS/SLIMOS"  
        if (entries[9]=="Shift Leader"):
            entries[9] = "Shift leader"    
        if (entries[9]=="Technical Coordinator"):
            entries[9] = "Technical coordinator"    
        if (entries[8]=="Other"):
            entries[8] = "N/A"
        if(entries[9]=="Other"):
            entries[9]="None"
        if(entries[7]=="Other"):
            entries[7]="Any"

        d={ 'entry':entries[0], 'view':entries[1], 'author':entries[2], 'start':entries[3], 'end':entries[4], 'subject':entries[5], 'text':entries[6], 'contact':entries[7], 'moyen':entries[8], 'system':entries[9], 'rest':entries[10] }


        print "contact   ", entries[7] # for me item_affected 
        print "moyen   ", entries[8] # for me subsystem
        print "system   ", entries[9] # for me contact
#########
        if (entries[7]=="Experiment"): 
            print "CASE1.1"
            if (entries[8]=="DSS"):
                elisa_insert = 'python /home/pmendez/workspace/python/elisa_client_api/elisa_insert.py -v 4 -o /home/pmendez/workspace/python/cookie.txt -s https://epdtdi-elog.cern.ch -j "{subject}" -a "{author}" -y "ATLAS" -b "{text}" -p "StartTime={start}" -p "EndTime={end}" -p "Contact={system}" -p "ATLAS_entry_type=Intervention" -p "ATLAS_subsystem=N/A" -x "closed" -e "DSS" -k "ProtoDUNE-SP"'.format(**d)
            else:
                print "CASE1.2"
                elisa_insert = 'python /home/pmendez/workspace/python/elisa_client_api/elisa_insert.py -v 4 -o /home/pmendez/workspace/python/cookie.txt -s https://epdtdi-elog.cern.ch -j "{subject}" -a "{author}" -y "ATLAS" -b "{text}" -p "StartTime={start}" -p "EndTime={end}"  -p "Contact={system}" -p "ATLAS_entry_type=Intervention" -p "ATLAS_subsystem={moyen}" -x "closed" -e "Experiment" -k "ProtoDUNE-SP"'.format(**d)

################
        elif (entries[7]=="To & CS"):
            print "CASE2"
            elisa_insert = 'python /home/pmendez/workspace/python/elisa_client_api/elisa_insert.py -v 4 -o /home/pmendez/workspace/python/cookie.txt -s https://epdtdi-elog.cern.ch -j "{subject}" -a "{author}" -y "ATLAS" -b "{text}" -p "StartTime={start}" -p "EndTime={end}"  -p "ATLAS_item_affected=Central Solenoid" -p "ATLAS_entry_type=Intervention" -p "ATLAS_item_affected=Toroid" -x "closed" -p "ATLAS_subsystem={moyen}" -e "Magnets" -p "Contact={system}" -k "ProtoDUNE-SP"'.format(**d)

########################
        elif (entries[9]=="Vacuum"):
            print "CASE4"
            elisa_insert = 'python /home/pmendez/workspace/python/elisa_client_api/elisa_insert.py -v 4 -o /home/pmendez/workspace/python/cookie.txt -s https://epdtdi-elog.cern.ch -j "{subject}" -a "{author}" -y "ATLAS" -b "{text}" -p "StartTime={start}" -p "EndTime={end}"  -p "Contact={system}" -p "ATLAS_subsystem=Vacuum" -p "ATLAS_entry_type=Intervention" -p "ATLAS_item_affected=VCS" -x "closed" -e "Magnets" -k "ProtoDUNE-SP"'.format(**d)

############################3
        elif (entries[8]=="DSS"):
            print "CASE5"
            elisa_insert = 'python /home/pmendez/workspace/python/elisa_client_api/elisa_insert.py -v 4 -o /home/pmendez/workspace/python/cookie.txt -s https://epdtdi-elog.cern.ch -j "{subject}" -a "{author}" -y "ATLAS" -b "{text}" -p "StartTime={start}"  -p "EndTime={end}"  -p "Contact={system}" -p "ATLAS_item_affected=Any" -p "ATLAS_entry_type=Intervention" -p "ATLAS_subsystem=N/A" -x "closed" -e "DSS" -k "ProtoDUNE-SP"'.format(**d)

##########
        else:
            print "ELSE3"
            elisa_insert = 'python /home/pmendez/workspace/python/elisa_client_api/elisa_insert.py -v 4 -o /home/pmendez/workspace/python/cookie.txt -s https://epdtdi-elog.cern.ch  -j "{subject}" -a "{author}" -y "ATLAS"  -b "{text}" -p "StartTime={start}" -p "EndTime={end}" -p "ATLAS_item_affected={contact}" -p "Contact={system}" -p "ATLAS_subsystem={moyen}" -x "closed" -p "ATLAS_entry_type=Intervention" -k "ProtoDUNE-SP" -e "Other"'.format(**d)


        print elisa_insert    

        if (entries[5] != "Titre"):
            cmd = subprocess.Popen(elisa_insert, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
            output = cmd.communicate()[0]
            print output            

            value_id=None

            for line in output.split('\n'):
                print line
                if line.startswith("id") and not line.startswith("idem") and line.find("None")==-1:
                    value_id = int(line.split(":")[1].lstrip())

            print "tttttttttttttttttttttttttttt", value_id
            if(value_id):
                elisa_update = 'python /home/pmendez/workspace/python/elisa_client_api/elisa_update.py -v 4 -o /home/pmendez/workspace/python/cookie.txt -s https://epdtdi-elog.cern.ch -k ProtoDUNE-SP -i %d -d "%s"'  %(value_id, d['end'])
                cmd2 = subprocess.Popen(elisa_update, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
                print elisa_update
                output2 = cmd2.communicate()[0]
                print output2


        print ("-------------------------------")

if __name__ == "__main__":
  main()
