#!/usr/bin/env python
# Created by P. Mendez 30/03/2020
# It include all cleaning procedures to prepare the inputs to include them in elog

import os
import sys, argparse
import shutil
import re

def replace(string, substitutions):

    substrings = sorted(substitutions, key=len, reverse=True)
    regex = re.compile('|'.join(map(re.escape, substrings)))
    return regex.sub(lambda match: substitutions[match.group(0)], string)

substitutions = {"\\"+"n": "", "xa0": "", "\\"+"xe8me": "eme", "\\": " ", "xe0": "e", "xe9": "e", "u'": "'", "[": "", "]": "", "u\"": ""}


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--infile', help="excel file to parse", default='', dest='inputfile')
    args = parser.parse_args()

    if not args.inputfile:
        raise RuntimeError("inputfile -i(--infile) option must be specified")


    fin = open(args.inputfile+".txt", "rt")
    fout = open(args.inputfile+"_out.txt","wt")
    for line in fin:
        output = replace(line,substitutions)
        fout.write(output)
    
    fin.close()
    fout.close()

    f = open(args.inputfile+"_out.txt", "rt")
    lines = f.readlines()

    f.close()

    sys.stdout = open(args.inputfile+"_elog.txt", 'w')

    for t in lines:

        s=t.replace("', ", "xx ")
        s2=s.replace(", '","xx ")
        s3=s2.split("xx")
        temp = s3[3].split(", ")
        del s3[3]

        s3.insert(3,temp[0])
        s3.insert(4,temp[1])

        for x in s3:

            if x.endswith('\''):
                x=x.replace('\'',"")
            if x.startswith(' \''):
                x=x.replace(' \'',"")
            print(x.lstrip())


if __name__ == "__main__":
  main()
