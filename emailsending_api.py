#!/usr/bin/env python

import os, sys
import subprocess
os.environ["TDAQ_PYTHONPATH"] = "/home/pmendez/workspace/python"
os.environ["PYTHONPATH"] = "/home/pmendez/workspace/python"
os.environ["USE_ELISA_MERGED"] = "true"

cookie = "/home/pmendez/workspace/python/cookie.txt"
url = "https://epdtdi-gral-elog.cern.ch"
subject = "\"test4\""
message_type = "\"Incident\""
body = "\"test body\""
author = "\"Patricia Mendez Lorenzo\""
entry_type = "\"DUNE\""
logbook = "\"epdtdi-gral\""

#bashCommand_remove = "rm %s" % (cookie)
#cmd = subprocess.Popen(bashCommand_remove, shell=True, stdout=subprocess.PIPE)

#bashCommand_cookie = "cern-get-sso-cookie --krb -r -u %s -o %s" % (url, cookie)
#cmd = subprocess.Popen(bashCommand_cookie, shell=True, stdout=subprocess.PIPE)

bashCommand_insert = "python /home/pmendez/workspace/python/elisa_client_api/elisa_insert.py -v 4 -o /home/pmendez/workspace/python/cookie.txt -s %s -j %s -y %s -b %s -a %s -e %s -k %s" %(url,subject,message_type,body,author,entry_type,logbook)
print bashCommand_insert
cmd = subprocess.Popen(bashCommand_insert, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
output = cmd.communicate()[0]
#print output

value_id=None

for line in output.split('\n'):
   if line.startswith("id") and line.find("None")==-1:
      value_id = int(line.split(":")[1].lstrip())

print value_id

new_date="\"17-05-2020 13:01:01\""

#bashCommand_update = "python /home/pmendez/workspace/python/elisa_client_api/elisa_update.py -o /home/pmendez/workspace/python/cookie.txt -s %s -k %s -i %d -d %s" %(url,logbook,value_id, new_date)
#print url, logbook, value_id, new_date
#cmd2 = subprocess.Popen(bashCommand_update, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
#output2 = cmd2.communicate()[0]

