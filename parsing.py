1;95;0c#!/usr/bin/env python
# Created by P. Mendez 21/04/2020
# It include parsing procedures needed before entering the data in the elog

import os
from datetime import datetime
from string import Template
import sys, argparse
import shutil
import re


from itertools import groupby

def breakby(biglist, sep, delim=None):
    for item in biglist:
        p = item.split(sep)
        yield p[0]
        if len(p) > 1:
            yield delim
            yield p[1]

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--infile', help="excel file to parse", default='', dest='inputfile')
    args = parser.parse_args()

    if not args.inputfile:
        raise RuntimeError("inputfile -i(--infile) option must be specified")

    f = open(args.inputfile+"_elog_ex.txt", "rt")
    lines = f.readlines()
    f.close()

    smallerlist = [list(g) for k,g in groupby(breakby(lines, 'Intervention_Atlas_2010', None),
                                          lambda x: x is not None) if k]


#    print lines

    filein = open('template.txt')
    src = Template( filein.read() )

    entry = lines[0]
    view = lines[1]
    author = lines[2]
    start = lines[3]
    end = lines[4]
    subject = lines[5]
    text = lines[6]
    contact = lines[7]
    moyen = lines[8]
    system = lines[9]
    rest = [lines[10], lines[11], lines[12]] 
    d={ 'entry':entry, 'view':view, 'author':author, 'start':start, 'end':end, 'subject':subject, 'text':text, 'contact':contact, 'moyen':moyen, 'system':system, 'rest':'\n'.join(rest) }
#do the substitution                                                                                                                                                                                                                                                    
    result = src.substitute(d)
    print result


      

#    f = open(args.inputfile+"_elog_ex.txt", "rt")
#    lines = f.readlines()
#    f.close()

#    count = 0
#    for t in lines:
#        print count, t
#        count +=1
        


if __name__ == "__main__":
  main()
