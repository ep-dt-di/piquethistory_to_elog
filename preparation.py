#!/usr/bin/env python
# Created by P. Mendez 30/03/2020
# Code intended to prepare the input file containing data using as input just the excel file

import os
import sys, argparse
import shutil
import xlrd
import cx_Oracle

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--infile', help="excel file to parse", default='', dest='inputfile')
    args = parser.parse_args()

    if not args.inputfile:
        raise RuntimeError("inputfile -i(--infile) option must be specified")

    loc = (args.inputfile)

    wb = xlrd.open_workbook(loc)
    sheet = wb.sheet_by_index(0)
    sheet.cell_value(0, 0)
    
#    sys.stdout = open(args.inputfile+'.txt', 'w')
    for i in range(sheet.nrows):
        print(sheet.row_values(i))
        print ("--------------------------------")

    with open(args.inputfile+'.txt', 'r') as fin:
        data = fin.read().splitlines(True)
    with open(args.inputfile+'.txt', 'w') as fout:
        fout.writelines(data[1:])

if __name__ == "__main__":
  main()
